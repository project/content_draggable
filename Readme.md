# Content Draggable

The "Content Draggable" module introduces an interactive and dynamic feature to
Drupal sites, allowing users to engage with web elements through a
click-and-drag interface.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/content_draggable).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/content_draggable).


## Table of contents

- Requirements
- Features
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [DraggableViews](https://www.drupal.org/project/draggableviews)


## Features

This module is designed to enhance user experience by making content more
interactive. With "Content Draggable," site builders and content managers can
easily define elements within their Drupal site that users can click on and move
around the screen.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

There is no configuration, the module will automatically create a custom view
for this module.


## Maintainers

- chetan - [chetan 11](https://www.drupal.org/u/chetan-11)
